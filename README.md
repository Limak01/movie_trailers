# Link
You can see app here: https://trailers-movie.netlify.app/

## Description
Movie trailers as the name suggests is a website where you choose a movie or tv series and you watch trailer of it. I've build it to better understand API calls and error handling in react js.

## Technologies i used
- HTML
- CSS
- React JS
- TMDB API
