import React, { useState } from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import Footer from './components/Footer/Footer'
import MovieList from './components/MovieList/MovieList'
import Navbar from './components/navbar/Navbar'
import Slider from './components/slider/Slider'
import requests from './requests'
import SingleMoviePage from './components/SingleMoviePage/SingleMoviePage'
import { LazyLoadComponent } from 'react-lazy-load-image-component'
import TopToday from './components/TopToday/TopToday'
import { ErrorBoundary } from 'react-error-boundary'
import Error404 from './components/Error404'
import Error from './components/Error'



function App() {
  const [displayedContent, setDisplayedContent] = useState('movies')

  const handleError = (error, errorInfo) => {
    console.log(error, errorInfo)
  }

  return (
    <Router>
        <div className="App dark-theme" >
          <Navbar setDisplayedContent={setDisplayedContent} displayedContent={displayedContent}/>
            <ErrorBoundary FallbackComponent={Error} onError={handleError}>
              <Switch>
                <Route exact path="/">
                  <Slider fetchGenres={requests.fetchGenres} fetchURL={requests.fetchTrending} baseURL={requests.baseURL} baseImgURL={requests.baseImgURL}/>
                  <div className="container main-container">
                    <MovieList displayedContent={displayedContent} sectionTitle={displayedContent === 'movies' ? 'Recommended' : 'Recommended'} fetchSeriesUrl={requests.fetchRecommendedSeries} fetchUrl={requests.fetchRecommendedMovies} baseUrl={requests.baseURL}/>
                    <TopToday baseUrl={requests.baseURL} baseImgUrl={requests.baseImgURL} fetchUrl={{day: requests.fetchTrendingDay, week: requests.fetchTrending, month: requests.fetchTrendingMonth}} />
                    <MovieList displayedContent={displayedContent} sectionTitle={displayedContent === 'movies' ? 'Top Rated' : 'Airing Today'} fetchSeriesUrl={requests.fetchAiringTodaySeries} fetchUrl={requests.fetchTopRatedMovies} baseUrl={requests.baseURL}/>
                  </div>
                </Route>
                <Route exact path="/:type/:id">
                  <LazyLoadComponent>
                    <SingleMoviePage fetchGenres={requests.fetchGenres} baseURL={requests.baseURL} basePosterUrl={requests.basePosterUrl} baseImgURL={requests.baseImgURL} API={requests.API} fetchTrendingDay={requests.fetchTrendingDay}/>
                  </LazyLoadComponent>
                </Route>
                <Route exact path="/404" status={404}>
                  <Error404 />
                </Route>
                <Route path="*" status={404}>
                  <Redirect to="/404" />
                </Route>
              </Switch>
            </ErrorBoundary>
          <Footer />
        </div>
    </Router>
  );
}

export default App;
