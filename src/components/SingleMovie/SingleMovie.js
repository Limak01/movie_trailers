import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './SingleMovie.css'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const BASE_IMG_URL = 'https://image.tmdb.org/t/p/w200/'



const SingleMovie = ({movie, movie_type}) => {
    const [isMouseOver, setIsMouseOver] = useState(false)
    const [date, setDate] = useState('')

    
    const handleDate = () => {
        if(movie_type === 'movie') {
            setDate(movie?.release_date?.split('-')[0])
        } else {
            setDate(movie?.first_air_date?.split('-')[0])
        }
    }

    useEffect(() => {
        handleDate()
    }, [movie])


    return (
        <>
            <Link to={`/${movie_type}/${movie?.id}`} >
                <div 
                    className="single-movie" 
                    onMouseOver={() => setIsMouseOver(true)} 
                    onMouseLeave={() => setIsMouseOver(false)}
                >
                    <div className="poster-image-holder">
                        {
                            isMouseOver ? 
                            <div class="on-movie-hover">
                                <div class="play-btn">
                                    <i className="fas fa-play"></i>
                                </div>
                            </div>
                            :
                            null
                        }
                        <LazyLoadImage effect="blur" src={movie.poster_path ? `${BASE_IMG_URL}${movie?.poster_path}` : './default-poster.png'} alt="poster" width="100%" className="sm-poster"/>
                    </div>
                    <div className="single-movie-details">
                        <div className="single-movie-title">
                            <h3>{movie?.title || movie?.original_name}</h3>
                        </div>
                        <div className="single-movie-more">
                            <span className="release-date">{date}</span>
                            <div className="type-of">
                                <span className="mv-type hp-type">{movie_type}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </>
    )
}

export default SingleMovie