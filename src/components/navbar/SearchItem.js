import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import './SearchItem.css'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const SearchItem = ({setOpenSearch, mobileType, baseImgUrl, type, id, title, poster_path, rating, setInputFocus, setSearchValue}) => {

    const handleClick = () => {
        if(mobileType) {
            setOpenSearch(false)
            document.querySelector('body').classList.remove('body-no-scroll')
        } else {
            setInputFocus(false)
            setSearchValue('')
        }
    }

    return (
        <>
            <Link to={`/${type}/${id}`} >
                <div 
                    className={`search-item ${mobileType && `mobile-search-item`}`} 
                    onClick={handleClick}>
                    <div className={`poster-holder ${mobileType && `mobile-poster-holder`}`}>
                        <LazyLoadImage
                            effect="blur" 
                            src={poster_path ? `${baseImgUrl}/w92${poster_path}` : `${process.env.PUBLIC_URL}/default-poster.png`}  
                            alt="movie poster" 
                            className="item-poster"
                        />
                    </div>
                    <div className={`item-details ${mobileType && `mobile-item-details`}`}>
                        <h3>{title}</h3>
                        <div className="item-rating">
                            <i className="fas fa-star"></i>
                            <span className="rating-number">{rating}</span>
                        </div>
                        <div className="media-type">
                            <span>{type}</span>
                        </div>
                    </div>
                </div>
            </Link>
        </>
    )
}

export default SearchItem