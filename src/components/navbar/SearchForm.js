import React, {useState, useEffect, useRef} from 'react'
import axios from 'axios'
import requests from '../../requests'
import SearchItem from './SearchItem'
import { LazyLoadComponent } from 'react-lazy-load-image-component'

const SearchForm = () => {
    const [searchValue, setSearchValue] = useState('')
    const [searchList, setSearchList] = useState([])
    const [inputFocus, setInputFocus] = useState(false)
    const divRef = useRef()



    const getSearchingItems = async (baseUrl, searchUrl, searchValue) => {
        try {
            const res = await axios.get(`${baseUrl}${searchUrl}${searchValue}`)
            setSearchList(res.data.results)
        } catch(error) {
            console.log(error)
        }
    }

    useEffect(() => {
        const checkIfClickedOutside = e => {
          
          if (inputFocus && divRef.current && !divRef.current.contains(e.target)) {
            setInputFocus(false)
            setSearchValue('')

          }
        }
        
        document.addEventListener("mousedown", checkIfClickedOutside)

        return () => {
            document.removeEventListener("mousedown", checkIfClickedOutside)
        }
    }, [inputFocus])

    useEffect(() => {
        if(searchValue != '')
            getSearchingItems(requests.baseURL, requests.searchUrl, searchValue)
    },[searchValue])
    
    return (
        <div className="form-container" ref={divRef}>
            <form>
                <input 
                    type="text" 
                    onFocus={() => setInputFocus(true)} 
                    placeholder="search" 
                    className="search-input" 
                    value={searchValue} 
                    onChange={(e) => setSearchValue(e.target.value)}
                />

                <button className="submit-button" onClick={(e) => e.preventDefault()}>
                    <i className="fas fa-search"></i>
                </button>
            </form>
            {
                searchValue.length > 0 && inputFocus === true ? 
                    <div className="proposed">
                        <div className="prop-pointer"></div>
                        <div className="proposed-items">
                        {
                            searchList.map((item, index) => {
                                /* return <h6>{item?.name || item?.title || item?.original_name}</h6> */
                                /* return <SearchItem key={index} baseImgUrl={requests.baseImgURL} title={item?.name || item?.title || item?.original_name} poster_path={item?.poster_path} rating={item?.vote_average} /> */

                                return item?.vote_average > 5 && 
                                    <LazyLoadComponent>
                                       <SearchItem 
                                            setInputFocus={setInputFocus}
                                            setSearchValue={setSearchValue}
                                            key={index} 
                                            baseImgUrl={requests.basePosterUrl} 
                                            id={item?.id} 
                                            type={item?.media_type} 
                                            title={item?.name || item?.title || item?.original_name} 
                                            poster_path={item?.poster_path} rating={item?.vote_average} 
                                        /> 
                                    </LazyLoadComponent>
                            })
                        } 
                        {
                            searchList.length === 0 
                            && 
                            <div className="nothing-found">
                                <h1>We dont find anything...</h1>
                                <iframe src="https://giphy.com/embed/3oEjI80DSa1grNPTDq" width="300"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                            </div>
                        }
                        </div>
                    </div>
                : null
            }
        </div>
    )
}

export default SearchForm