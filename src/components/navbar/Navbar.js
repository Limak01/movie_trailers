import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import ThemeSwitch from '../ThemeSwitch'
import MobileSearch from './MobileSearch'
import './Navbar.css'
import NavLink from './NavLink/NavLink'
import SearchForm from './SearchForm'

const Navbar = ({setDisplayedContent, displayedContent}) => {

    const [openSearch, setOpenSearch] = useState(false)
    
    const clickHandler = () => {
        if(displayedContent === 'movies') {
            setDisplayedContent('tv')
        } else {
            setDisplayedContent('movies')
        }
    }

    return (
        <header>
            {openSearch ? <MobileSearch setOpenSearch={setOpenSearch}/> : null}
            <div className="nav-bar">
                <div className="container nav-container">
                    <div className="left-side">
                        <Link to="/">
                            <div className="logo-container">
                                <span className="logo">m<span className="blue-color">T</span></span>
                            </div>
                        </Link>
                        <nav className="nav-selector">
                            <ul className="nav-links">
                                {/* <li onClick={() => setDisplayedContent('movies')}>
                                    <Link to="/">Movies</Link>
                                </li>
                                <li onClick={() => setDisplayedContent('tv')} >
                                    <Link to="/">TV-Series</Link>
                                </li> */}
                                {/* <li 
                                    onClick={() => setDisplayedContent('movies')}
                                    onMouseOver={() => {
                                        setIsMovieLinkHovered(true)
                                        setIsMovieLinkOut(false)
                                    }}
                                    onMouseOut={() => {
                                        setIsMovieLinkHovered(false)
                                        setIsMovieLinkOut(true)
                                    }}
                                >
                                    <i 
                                        className={`fas fa-video ${isMovieLinkHovered ? `link-label-is-hovered` : null} ${isMovieLinkOut ? `link-label-out` : null}`}
                                    ></i>
                                    <span 
                                        className={`link-label ${isMovieLinkHovered ? `link-is-hovered` : null} ${isMovieLinkOut ? `link-out` : null}`}
                                    >
                                        MOVIES
                                    </span>
                                </li> */}
                                <NavLink icon="movie" label="MOVIES" setDisplayedContent={setDisplayedContent}/>
                                <NavLink icon="tv" label="SERIES" setDisplayedContent={setDisplayedContent}/>
                            </ul>
                        </nav>
                        <div className="mobile-menu">
                            <div className="theme-switch-holder">
                                <ThemeSwitch />
                            </div>
                            {/* <div className="separator"/> */}
                            <div 
                                className="search-button" 
                                onClick={() => {
                                    setOpenSearch(true)
                                    document.querySelector('body').classList.add('body-no-scroll')
                                }}
                            >
                                <i className="fas fa-search"></i>
                            </div>
                            <div className="hamburger" onClick={clickHandler}>
                                    {
                                        displayedContent === 'movies' 
                                        ?
                                        <i className="fas fa-video"></i>
                                        :
                                        <i className="fas fa-tv"></i>
                                    }
                            </div>
                        </div>
                    </div>
                    <div className="right-side">
                        <div className="theme-switch-holder">
                            <ThemeSwitch />
                        </div>
                        <SearchForm />
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Navbar