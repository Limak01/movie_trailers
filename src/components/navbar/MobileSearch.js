import React, { useState, useEffect } from 'react'
import reactDom from 'react-dom'
import './MobileSearch.css'
import requests from '../../requests'
import axios from 'axios'
import SearchItem from './SearchItem'

const MobileSearch = ({setOpenSearch}) => {
    const [searchValue, setSearchValue] = useState('')
    const [searchList, setSearchList] = useState([])

    const getSearchingItems = async (baseUrl, searchUrl, searchValue) => {
        try {
            const res = await axios.get(`${baseUrl}${searchUrl}${searchValue}`)
            setSearchList(res.data.results)
        } catch(error) {
            console.log(error)
        }
    }

    useEffect(() => {
        if(searchValue != '')
            getSearchingItems(requests.baseURL, requests.searchUrl, searchValue)
    },[searchValue])

    return reactDom.createPortal(
        <div className="mobile-search-container">
            <div 
                className="mobile-search-close-button"
                onClick={() => {
                    setOpenSearch(false)
                    document.querySelector('body').classList.remove('body-no-scroll')
                }}
            >
                <i className="fas fa-times"></i>
            </div>
            <div className="mobile-search-form">
                <input 
                    type="text" 
                    className="mobile-search-input" 
                    placeholder="Search"
                    value={searchValue}
                    onChange={e => setSearchValue(e.target.value)}
                />
            </div>
            <div className="mobile-search-results">
            {
                searchList.map((item, index) => {
                    return item?.vote_average > 5 && 
                            <SearchItem 
                                setOpenSearch={setOpenSearch}
                                mobileType={true}
                                key={index} 
                                baseImgUrl={requests.basePosterUrl} 
                                id={item?.id} 
                                type={item?.media_type} 
                                title={item?.name || item?.title || item?.original_name} 
                                poster_path={item?.poster_path} 
                                rating={item?.vote_average} 
                            /> 
                })
            } 
            </div>
        </div>
        , document.getElementById('mobile-page')
    )
}

export default MobileSearch