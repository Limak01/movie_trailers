import { useState } from "react"
import { Link } from "react-router-dom"
import "./NavLink.css"

const NavLink = ({icon, label, setDisplayedContent}) => {
    const [isLinkHovered, setIsLinkHovered] = useState(false)
    const [isLinkOut, setIsLinkOut] = useState(false)

    return (
        <Link to="/">
            <li 
                onClick={() => icon === 'movie' ? setDisplayedContent('movies') : setDisplayedContent('tv')}
                onMouseOver={() => {
                    setIsLinkHovered(true)
                    setIsLinkOut(false)
                }}
                onMouseOut={() => {
                    setIsLinkHovered(false)
                    setIsLinkOut(true)
                }}
                className="single-link"
            >
                <i 
                    className={`fas fa-${icon === 'movie' ? `video` : `tv`} ${isLinkHovered ? `link-label-is-hovered` : null} ${isLinkOut ? `link-label-out` : null}`}
                ></i>
                <span 
                    className={`link-label ${isLinkHovered ? `link-is-hovered` : null} ${isLinkOut ? `link-out` : null}`}
                >
                    {label}
                </span>
            </li>
        </Link>
    )
}

export default NavLink