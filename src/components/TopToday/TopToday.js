import React, { useState, useEffect } from 'react'
import { getMovies } from '../../api_calls'
import { Link } from 'react-router-dom'
import './TopToday.css'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { useErrorHandler } from 'react-error-boundary';

const BASE_IMG_URL = 'https://image.tmdb.org/t/p/w154'

const TopToday = ({ baseUrl, fetchUrl, baseImgUrl}) => {
    const [activeSwitch, setActiveSwitch] = useState(0)
    const [activeBackground, setActiveBackground] = useState(0)
    const [movieList, setMovieList] = useState([])
    const handleError = useErrorHandler()

    useEffect(() => {
        if(activeSwitch === 0) {
            getMovies(baseUrl, fetchUrl.day)
                .then(res => setMovieList(res))
                .catch(error => handleError(error))
        } else if(activeSwitch === 1) {
            getMovies(baseUrl, fetchUrl.week)
                .then(res => setMovieList(res))
                .catch(error => handleError(error))
        }
    }, [activeSwitch])


    return (
        <div 
            className="top-today"
            style={{backgroundImage: `url('${baseImgUrl}${movieList[activeBackground]?.backdrop_path}')`}}
        >
            <div className="top-today-dark" >
                <div className="top-today-container">
                    <div className="top-today-top">
                        <h1 className="top-today-title">Top 5 </h1>
                        <div className="top-today-switch">
                            <div 
                                className={`switch ${activeSwitch === 0 ? 'active-switch' : null}`}
                                onClick={() => setActiveSwitch(0)}
                            >
                                <span>Day</span>
                            </div>
                            <div 
                                className={`switch ${activeSwitch === 1 ? 'active-switch' : null}`}
                                onClick={() => setActiveSwitch(1)}
                            >
                                <span>Week</span>
                            </div>
                        </div>
                    </div>
                    <div className="top-today-movies-container">
                        <div className="top-today-movies">
                            {
                                movieList.map((item, index) => {
                                    return index < 5 && (
                                        <React.Fragment key={index}>
                                            <Link to={`/${item?.media_type}/${item?.id}`}>
                                                <div className="top-today-single" onMouseOver={() => setActiveBackground(index)}>
                                                    <div className="top-today-image">
                                                        <LazyLoadImage effect="blur" src={`${BASE_IMG_URL}${item?.poster_path}`} className="top-today-poster" width="100%"/>
                                                    </div>
                                                    <div className="top-today-poster-title">
                                                        <h3>{item?.original_name || item?.title}</h3>
                                                    </div>
                                                </div>
                                            </Link>
                                        </React.Fragment>
                                    )
                                })
                            } 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TopToday