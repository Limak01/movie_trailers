import React, { useState, useEffect, useRef } from 'react'
import { Link } from 'react-router-dom';
import './Slider.css'
import { getMovies, getGenres } from '../../api_calls'

const DELAY = 20000;

const Slider = ({fetchURL, baseURL, baseImgURL, fetchGenres}) => {
    const [slideNumber, setSlideNumber] = useState(0);
    const [trendingMoviesList, setTrendingMoviesList] = useState([])
    const [movieGenres, setMovieGenres] = useState([])
    const [seriesGenres, setSeriesGenres] = useState([])
    const [slideLineCount, setSlideLineCount] = useState([1,2,3,4,5])
    const timeoutRef = useRef(null);

    const resetTimeout = () => {
        if(timeoutRef.current) {
            clearTimeout(timeoutRef.current)
        }
    }

    useEffect(() => {

        resetTimeout()

        timeoutRef.current = setTimeout(
            () => {
                setSlideNumber(prevSlide => {
                    if(prevSlide === 4) {
                        return 0
                    } else return prevSlide + 1
                })
            },
            DELAY
        )

        return () => resetTimeout()

    }, [slideNumber])

    useEffect(() => {
        getMovies(baseURL, fetchURL)
            .then(res => setTrendingMoviesList(res))


        getGenres(baseURL, fetchGenres)
            .then(({movie_genres, tv_genres}) => {
                setMovieGenres(movie_genres)
                setSeriesGenres(tv_genres)
            })
    },[])

    return (
        <div className="slider">
            <div 
                className="background-image-container"
                style={{backgroundImage: `url("${baseImgURL}${trendingMoviesList[slideNumber]?.backdrop_path}")`}}
            >
                <div className="dark" >
                    <div className="container slider-container">
                        <div className="movie-info-container">
                            <p className="slider-title">{trendingMoviesList[slideNumber]?.title || trendingMoviesList[slideNumber]?.original_name}</p>
                            <div className="movie-details">
                                <div className="movie-rating">
                                    <i className="fas fa-star"></i>
                                    <span className="rating">{trendingMoviesList[slideNumber]?.vote_average}</span>
                                </div>
                                <div className="movie-type">
                                    {
                                        trendingMoviesList[slideNumber]?.genre_ids.map((id, index) => {
                                            const genreForMovie = movieGenres.find(item => item.id == id)
                                            const genreForSeries = seriesGenres.find(item => item.id == id)
                                            return <span 
                                                key={index} 
                                                className="type-name"
                                                >{trendingMoviesList[slideNumber]?.media_type === 'movie'? genreForMovie?.name : genreForSeries?.name}
                                                </span>
                                        })
                                    }
                                </div>
                            </div>
                            <div className="movie-desc">
                                <p>
                                    {trendingMoviesList[slideNumber]?.overview}
                                </p>
                            </div>
                            <Link to={`/${trendingMoviesList[slideNumber]?.media_type}/${trendingMoviesList[slideNumber]?.id}}`} >
                                <button className="watch-button">
                                    <i className="fas fa-play"></i>
                                    <span className="button-title">Watch Now</span>
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="slider-nav">
            
                {slideLineCount.map((_, index) => {
                   return (
                   <div 
                        key={index} 
                        className={`slide-line${slideNumber === index ? " active-slide" : ""}`}
                        onClick={() => setSlideNumber(index)}
                    ></div>
                   )
                })}
            </div>
        </div>
    )
}

export default Slider