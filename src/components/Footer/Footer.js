import React from 'react'
import './Footer.css'

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container footer-container">
                <span className="lg">
                    m
                    <span className="logo-color">T</span>
                </span>
                <span className="copyright">
                    &#169; 2021
                </span>
                <span className="my-name">
                    Limak
                </span>
            </div>
        </footer>
    )
}

export default Footer
