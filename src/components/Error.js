import React from 'react'
import { Link } from 'react-router-dom'

const ERROR_STYLES = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '70px 0'
}


const Error = () => {
    return (
        <div className="container" style={ERROR_STYLES}>
            <h1>Ups! We have some error.</h1>
            <img src="./errorImage.png" alt="error" width="600px"/>
        </div>
    )
}

export default Error