import React, {useState, useEffect} from 'react'
import { Link, Redirect, useParams } from 'react-router-dom'
import axios from 'axios'
import './SingleMoviePage.css'
import movieTrailer from 'movie-trailer'
import SingleMovie from '../SingleMovie/SingleMovie'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { getMovies, getGenres } from '../../api_calls'
import { useErrorHandler } from "react-error-boundary"
import Details from './Details/Details'
import Credits from './Credits/Credits'


//suicide squad id = 436969

const SingleMoviePage = ({fetchGenres, basePosterUrl,baseURL, baseImgURL, API, fetchTrendingDay}) => {
    let { id, type } = useParams()
    const [movieID, setMovieID] = useState('')
    const [movie, setMovie] = useState([])
    /* const [trending, setTrending] = useState([]) */
    const [movieGenres, setMovieGenres] = useState([])
    const [seriesGenres, setSeriesGenres] = useState([])
    const handleError = useErrorHandler()
    const [isError, setIsError] = useState(false)

    const getMovie = async (baseURL, type, id, API) => {
        try {
            const result = await axios.get(`${baseURL}/${type}/${id}?api_key=${API}`)
            movieTrailer(result.data?.original_name || result.data?.title , {id: true, multi: true})
                .then(res => setMovieID(res))
            setMovie(result.data) 
        } catch(error) {
            setIsError(true)
        }
    }

    useEffect(() => {
        getGenres(baseURL, fetchGenres)
            .then(({movie_genres, tv_genres}) => {
                setMovieGenres(movie_genres)
                setSeriesGenres(tv_genres)
            })
            .catch(err => {
                handleError(err)
            })
    },[])

    useEffect(() => {
        //Get single movie from api
        getMovie(baseURL, type, id, API)

        //Get list of trending movies from api 
        /* getMovies(baseURL, fetchTrendingDay)
            .then(res => setTrending(res))
            .catch(err => {
                handleError(err)
            }) */

        if(window.scrollY) {
            window.scroll(0,0)
        }
    },[id, type])


    if(isError) {
        return <Redirect to="/404" />
    }

    return (
        <div className="single-page">
            <Details movie={movie} type={type} genres={{movie: movieGenres, tv: seriesGenres}} movieID={movieID}/>
            <Credits id={id} type={type}/>
        </div>
    )
}

export default SingleMoviePage