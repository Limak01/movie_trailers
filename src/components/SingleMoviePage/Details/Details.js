import React, { useState, useEffect } from 'react'
import './Details.css'
import requests from '../../../requests'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import Player from './Player';

const Details = ({movie, type, movieID}) => {
    const [playTrailer, setPlayTrailer] = useState(false)

    const dateMovie = movie?.release_date?.split('-')[0]
    const dateTv = movie?.first_air_date?.split('-')[0]

    useEffect(() => setPlayTrailer(false), [movie?.id])

    return (
        <div 
            className="details"
            /* style={{backgroundImage: `url('${requests.baseImgURL}${movie?.backdrop_path}')`}} */
        >
            <div className="details-background">
                <div className="details-bg-color"/>
                <div 
                    className="details-backdrop" 
                    style={{backgroundImage: `url('${requests.baseImgURL}${movie?.backdrop_path}')`}}
                >
                    <div className="backdrop-color">
                        <div className="backdrop-gradient" />
                    </div>
                </div>
            </div>
            <div className="container details-container">
                <div className="details-poster">
                    <LazyLoadImage 
                        effect="blur" 
                        src={`${requests.basePosterUrl}/w500/${movie?.poster_path}`}
                        width="100%"
                        className="details-poster-image"
                    />
                </div>
                { playTrailer ? 
                    <Player movieID={movieID} title={movie?.title || movie?.original_name} setPlayTrailer={setPlayTrailer}/> 
                :
                <div className="details-details">
                    <div className="details-title-date">
                        <p className="details-title">{movie?.title || movie?.original_name}<span className="details-date">({type === 'movie' ? dateMovie : dateTv})</span></p>
                    </div>
                    <div className="details-more">
                        <span className="details-type">{type}</span>
                        <span className="details-date-full">{movie?.first_air_date || movie?.release_date}</span>
                        <div className="genres">
                        {
                        movie?.genres?.map((item, index) => {
                                return  <span 
                                            key={index} 
                                            className="single-genre"
                                        >{item?.name}{index === movie?.genres.length - 1 ? null : ', '}</span>
                            })
                        }
                        </div>
                    </div>
                    <div className="play-trailer">
                        <p className="play-trailer-button" onClick={() => setPlayTrailer(true)}>
                            <i className="fas fa-play"></i>
                            <span>Play Trailer</span>
                        </p>
                    </div>
                    {   
                        movie?.tagline ?
                            <div className="tagline">
                                <p>”{movie?.tagline}„</p>
                            </div>
                        : null
                    }
                    <div className="description">
                        <h3>Description</h3>
                        <p>
                            {
                                movie?.overview
                            }
                        </p>
                    </div>
                </div>
                }
            </div>
        </div>
    )
}

export default Details