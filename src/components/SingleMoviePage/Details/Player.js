import React, { useState } from 'react'

const Player = ({movieID, title, setPlayTrailer}) => {
    const [actualID, setActualID] = useState(movieID ? movieID[movieID.length - 1] : [])
    return (
        <div className="player-container">
            <div className="player-back-btn">
                <div className="back-btn" onClick={() => setPlayTrailer(false)}>
                    <i class="fas fa-arrow-left"></i>
                </div>
                <h3>{title}</h3>
            </div>
            <div className="player">
                <iframe 
                    width="700px" 
                    height="400px" 
                    src={`https://www.youtube.com/embed/${actualID}`}
                    title="YouTube video player" 
                    frameborder="0" 
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen" 
                    className="yt-player"
                    >
                </iframe>
            </div>
            <div className="trailer-links">
                {
                    movieID ? movieID.slice(0).reverse().map((item, index) => {
                        return index < 10 && (
                            <div className="change-trailer-btn" onClick={() => setActualID(item)}>
                                <span>Link {index + 1}</span>
                            </div>
                        )
                    }) :
                    <div className="change-trailer-btn">
                        <span>We didn't find any trailer for this movie.</span>
                    </div>
                }
            </div>
        </div>
    )
}

export default Player