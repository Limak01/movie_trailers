import React, {useState, useEffect, useRef} from 'react'
import './Credits.css'
import requests from '../../../requests'
import { getMovies } from '../../../api_calls'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const SCROLL_WIDTH = 800

const Credits = ({id, type}) => {
    const [casts, setCasts] = useState([])
    const scroll = useRef()
    const castContainer = useRef()
    const [scrollPosition, setScrollPosition] = useState(0)

    const FETCH_CASTS = `/${type}/${id}/credits?api_key=${requests.API}&language=en-US`

    const handleRightScroll = () => {
        setScrollPosition(prevRef => {
            if(prevRef + SCROLL_WIDTH >= castContainer.current.offsetWidth - SCROLL_WIDTH) {
                return castContainer.current.offsetWidth - SCROLL_WIDTH
            }
            return prevRef + SCROLL_WIDTH
        })
    }

    const handleLeftScroll = () => {
        setScrollPosition(prevRef => {
            if(prevRef - SCROLL_WIDTH <= 0) {
                return 0
            }
            return prevRef - SCROLL_WIDTH
        })
    }

    useEffect(() => {
        scroll.current.scroll(scrollPosition, 0)
    }, [scrollPosition])

    useEffect(() => {
        getMovies(requests.baseURL, FETCH_CASTS)
            .then(res => setCasts(res))
    }, [id])

    return (
        <div className="credits">
            <div className="container">
                <h1 className="credits-title">Cast <span className="cast-found">({casts?.cast?.length})</span></h1>
                <div className="btns-holder">
                    <div className="credits-cast-container" ref={scroll}>
                        <div className="cast-list" ref={castContainer}>
                        {
                            casts?.cast?.length > 0 ? 
                                casts?.cast?.map((item, index) => {
                                    return (
                                        <div className="single-cast" key={index}>
                                            <div className="cast-image">
                                                <LazyLoadImage 
                                                    effect="blur"
                                                    src={item.profile_path ? `${requests.basePosterUrl}/w200/${item?.profile_path}` : `${process.env.PUBLIC_URL}/default-poster.png`}
                                                    width="100%"
                                                    className="cast-prof"
                                                />
                                            </div>
                                            <div className="cast-details">
                                                <h3 className="cast-name">{item?.name}</h3>
                                                <p className="cast-character">{item?.character}</p>
                                            </div>
                                        </div>
                                    )
                                })
                            : 
                            <div className="no-cast-found">
                                <h1>No cast found</h1>
                            </div>
                        }
                        </div>
                    </div>
                    {
                        castContainer.current?.offsetWidth < scroll.current?.offsetWidth ? null
                        : <>
                            <div className="scroll-right scroll" onClick={handleRightScroll}>
                                <i className="fas fa-caret-right"></i> 
                            </div>
                            <div className="scroll-left scroll" onClick={handleLeftScroll}>
                                <i className="fas fa-caret-left"></i> 
                            </div>
                        </>
                    }
                </div>
            </div>
        </div>
    )
}

export default Credits