import React from 'react'

const ERROR_STYLES = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '140px 0 40px 0'
}



const Error404 = () => {
    return (
        <div className="container" style={ERROR_STYLES}>
            <h1>404 Page not found.</h1>
            <img src="./errorImage.png" alt="error" width="600px" className="err404"/>
        </div>
    )
}


export default Error404