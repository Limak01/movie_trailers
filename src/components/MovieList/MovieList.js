import React, {useState, useEffect} from 'react'
import SingleMovie from '../SingleMovie/SingleMovie'
import './MovieList.css'
import { LazyLoadComponent } from 'react-lazy-load-image-component'
import { getMovies } from '../../api_calls'
import { useErrorHandler } from "react-error-boundary"


const MovieList = ({displayedContent, sectionTitle, fetchUrl, baseUrl, fetchSeriesUrl}) => {
    const [moviesList, setMoviesList] = useState([])
    const handleError = useErrorHandler()

    useEffect(() => {
        if(displayedContent === 'movies') {
            getMovies(baseUrl, fetchUrl)
                .then(res => {
                    setMoviesList(res)
                })
                .catch(err => {
                    handleError(err)
                }) 
        } else {
            getMovies(baseUrl, fetchSeriesUrl)
                .then(res => setMoviesList(res))
                .catch(err => {
                    handleError(err)
                }) 
        }
    }, [displayedContent])

    return (
        <div className="movies-container">
            <div className="section-title">
                <h1>{sectionTitle}</h1>
                <div className="title-underline"></div>
            </div>
            <div className="select-movie">
                {
                    moviesList.map((movie, index) => {
                        return (
                            <LazyLoadComponent key={index}>
                                <SingleMovie movie={movie} movie_type={displayedContent === 'movies'? 'movie' : 'tv'}/>
                            </LazyLoadComponent>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default MovieList