import { useState, useEffect } from "react"


const DARK_THEME_STYLES = {
    background: 'none',
    outline: 'none',
    border: 'none',
    padding: '5px 10px',
    fontSize: '21px',
    cursor: 'pointer',
    color: "var(--font-color)",
}

const LIGHT_THEME_STYLES = {
    background: 'none',
    outline: 'none',
    border: 'none',
    padding: '5px 10px',
    fontSize: '21px',
    cursor: 'pointer',
    color: "#272727",
}


const ThemeSwitch = () => {
    const [theme, setTheme] = useState('light')
    const body = document.body

    const changeTheme = () => {
        if(theme === 'light') {
            localStorage.setItem('theme', 'dark')
            setTheme('dark')
            body.classList.add('dark-theme')
            body.classList.remove('light-theme')
        } else {
            localStorage.setItem('theme', 'light')
            setTheme('light')
            body.classList.add('light-theme')
            body.classList.remove('dark-theme')
        }
    }

    useEffect(() => {
        if(localStorage.getItem('theme') != null) {
            setTheme(localStorage.getItem('theme'))
            changeTheme();
        }
    }, [])

    return (
        <button style={theme === 'light' ? LIGHT_THEME_STYLES : DARK_THEME_STYLES} onClick={changeTheme} title={theme === 'light' ? 'Dark theme' : 'Light theme'}>
            {
                theme === 'light' ?
                    <i className="fas fa-moon"></i>
                :
                    <i className="fas fa-sun"></i>
            }
        </button>
    )
}

export default ThemeSwitch