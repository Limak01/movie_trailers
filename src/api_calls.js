import axios from "axios"


export const getMovies = async (baseUrl, fetchUrl) => {
    try {
        const res = await axios.get(`${baseUrl}${fetchUrl}`)
        
        return res?.data?.results || res?.data
    } catch(err) {
        return Promise.reject(new Error(err.message))
    }
} 

export const getGenres = async (baseUrl, fetchUrl) => {
    try {
        const movie_genres = await axios.get(`${baseUrl}${fetchUrl.movies}`)
        const tv_genres = await axios.get(`${baseUrl}${fetchUrl.series}`)
        
        return {
            movie_genres: movie_genres.data.genres,
            tv_genres: tv_genres.data.genres
        }
    } catch(err) {
        return Promise.reject(new Error(err.message))
    }
}